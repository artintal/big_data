if __name__ == '__main__':
    from pyspark import SparkContext, SparkConf
    conf = SparkConf().setAppName('Task5').setMaster(u'local[8]')
    sc = SparkContext(conf=conf)

from operator import itemgetter
from scipy import stats

filename = "file:///users/banthony/cs567/data/ExAC.r0.1.sites.vep.vcf"
data = sc.textFile(filename).cache()

# Rows beginning with # are not data, they're comments
rows = data.filter(lambda line: not line.startswith('#'))

# Keep all columns for now
columns = rows.map(lambda line: line.split('\t'))

# Functions
name = lambda data: '{chrom}:{pos}'.format(chrom=data[0], pos=data[1])
swap = lambda x: (x[1], x[0])
loss_of_function = lambda data: any(len(d.split('|')[-2]) > 0 for d in data[-1].split(',') if len(d.split('|')) > 1)

# Most Variability
# Key is chrom:pos
mv = columns.map(lambda data: (name(data), 1)).reduceByKey(lambda v,v2: v+v2).sortByKey()
mv.persist()

# Loss of Function
# Key is chrom:pos
# Value is loss of function count
lof = columns.map(lambda data: (name(data), 1 if loss_of_function(data) else 0)).reduceByKey(lambda v,v2: v+v2).sortByKey()
lof.persist()

print('There are %d loss of function variations out of %d total variations' % (lof.filter(lambda x: x[1] > 0).count(), lof.count()))
# There are 33623 loss of function variations out of 9578488 total variations

# We want to create a histogram of where the mutations occur
# For now, we are just creating 10 equal bins. Eventually, we will define these bins to be the types of genes (immune system, hox, etc).
# Need to convert key to number first!
# These check for failures in conversion. If it is X or Y, return 23
def gene_number(data):
    try:
        return int(data[0])
    except ValueError:
        if data[0] == 'X' or data[0] == 'Y':
            return 23
        return -1

# We are currently just reading the position number, but we need to include gene number somehow....
def pos_number(data):
    try:
        return int(data[1])
    except ValueError:
        return -1

mv_chrom = columns.map(gene_number).countByValue()
lof_chrom = columns.map(lambda data: gene_number(data) if loss_of_function(data) else -1).filter(lambda v: v >= 0).countByValue()

print('Variation chromosome counts')
for k, v in mv_chrom.items():
    print('%d: %d' % (k, v))

print('\nLoss of function histogram')
for k, v in lof_chrom.items():
    print('%d: %d' % (k, v))

mv_values = mv_chrom.values()
lof_values = lof_chrom.values()
D, pvalue = stats.ks_2samp(mv_values, lof_values)
print('KS statistic = %g, pvalue = %g' % (D, pvalue))

mv_gen_pos = columns.map(lambda data: (gene_number(data), pos_number(data)))
lof_gen_pos = columns.map(lambda data: (gene_number(data), pos_number(data) if loss_of_function(data) else -1))
for chrom in range(1, 24):
    # Key is chromosome, Value is position. Filter by chromosome and values >= 0. Then keep only the values
    mv_pos = mv_gen_pos.filter(lambda t: t[0] == chrom and t[1] >= 0).values()
    lof_pos = lof_gen_pos.filter(lambda t: t[0] == chrom and t[1] >= 0).values()

    D, pvalue = stats.ks_2samp(mv_values, lof_values)
    print('Chromosome %d: KS statistic = %g, pvalue = %g' % (chrom, D, pvalue))

    if mv_pos.count() > 0:
        mvp_buckets, mvp_hist = mv_pos.histogram(100)
        print('\nChromosome %d: Variation Position histograms' % (chrom))
        for i, v in enumerate(mvp_hist):
            print('[%d,%d%c: %d' % (mvp_buckets[i], mvp_buckets[i+1], ']' if i+1 > len(mvp_hist) else ')', v))
    if lof_pos.count() > 0:
        lofp_buckets, lofp_hist = lof_pos.histogram(mvp_buckets)
        print('\nChromosome %d: Loss of function Position historgram' % (chrom))
        for i, v in enumerate(lofp_hist):
            print('[%d,%d%c: %d' % (lofp_buckets[i], lofp_buckets[i+1], ']' if i+1 > len(lofp_hist) else ')', v))
