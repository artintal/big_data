From task1 directory. Open up the spark shell by typing:

  spark-shell

in the command line. Then, when it prompts you for code, type:

  :load count_least_mutated.scala

It should put the output in task2 in the hdfs dfs. To retrieve, type:

  hdfs dfs -get task2 .

This will add the results to the task2 directory. 
