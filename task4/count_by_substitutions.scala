import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf

//load the file
val file = sc.textFile("ExAC.r0.1.sites.vep.vcf")

//filter out the comments that start with '#'
val fileFiltered = file.filter(!_.startsWith("#"))

//split the rows by tab and only grab the elements in cols 3 and 4.
//these will be the original letter and the subsituted letter(s)
val rows = fileFiltered.map(line => line.split("\t").slice(3, 5))

//filter only the results that contain single letters
val r1 = rows.filter(line => line(0).length < 2).filter(line => line(1).length < 2)

//create key,value pairs and count the unique keys
val result = r1.map(data => (data(0)+" -> "+data(1), 1)).reduceByKey(_ + _)

//sort the results by value
val sorted_result = result.map(x => x.swap).sortByKey(false, 1).map(x => x.swap)

//save sorted results as text file
sorted_result.saveAsTextFile("task4")
