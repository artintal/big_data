/////////////////////////////////////////////
//Task 2: Find the least mutated pairs.
/////////////////////////////////////////////

//Import important stuff
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf

//Open the file
val file = sc.textFile("ExAC.r0.1.sites.vep.vcf")

//Eliminate the comments at top of file. They are for humans, not scripts.
val rows = file.filter(!_.startsWith("#"))

def indexOneOrEmptyString(arr:Array[String]) : String = {
  if(arr.size > 1){
    return arr(1);
  } else {
    return "";
  }
}

//Because we are counting the mutations, we only care about the chromosome and position on the chromosome (first two columns)
val geneNameArrays = rows.map(x => x.split("\t")(7).split(";")(x.split("\t")(7).split(";").size-1).split("\\|"))
val geneNames = geneNameArrays.map(x => indexOneOrEmptyString(x))


//We map the chromosome and position as a key with a value of 1. We reduce by key to get a count of mutated pairs, and sort by ascending value to get least mutated.
val sorted_result = geneNames.map(gene => (gene, 1)).reduceByKey((a,b) => a+b).map(x => x.swap).sortByKey(true,1).map(x => x.swap)

//Finally, we save the result as a test file into a task1 folder in the hdfs dfs
sorted_result.saveAsTextFile("task2")
