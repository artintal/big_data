
/////////////////////////////////////////////
//Task 3: Find the common mutation per location
/////////////////////////////////////////////

//Import important stuff
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import scala.tools.nsc.io.File

//Open the file
val file = sc.textFile("ExAC.r0.1.sites.vep.vcf")

//Eliminate the comments at top of file. They are for humans, not scripts.
val rows = file.filter(!_.startsWith("#"))

val mutations = rows.map(_.split("\t").take(5))


//These filters will get the total number of insertions, deletions, and substitutions
//TODO: apply map to do over chromosome and position
val insertions = mutations.filter(x => x(4).contains(x(3)))
val deletions = mutations.filter(x => x(3).length() > x(4).length())
val substitutions = mutations.filter(x => x(3).length() == x(4).length())

//Map over chromosome
val ins_result = insertions.map(data => (data(0) -> 1)).reduceByKey((a,b)=> a + b).sortByKey(false,1)
val sub_result = substitutions.map(data => (data(0) -> 1)).reduceByKey((a,b)=> a + b).sortByKey(false,1)
val del_result = deletions.map(data => (data(0) -> 1)).reduceByKey((a,b)=> a + b).sortByKey(false,1)

var del:Array[Int] = new Array[Int](23)
var sub:Array[Int] = new Array[Int](23)
var ins:Array[Int] = new Array[Int](23)

//Put result of map into corresponding arrays
del_result.collect().foreach { x => if (x._1 == "X") {del(22) = (x._2)} else {del(x._1.toInt-1) = (x._2) }}
ins_result.collect().foreach { x => if (x._1 == "X") {ins(22) = (x._2)} else {ins(x._1.toInt-1) = (x._2) }}
sub_result.collect().foreach { x => if (x._1 == "X") {sub(22) = (x._2)} else {sub(x._1.toInt-1) = (x._2) }}

val sub_out = "Substitutions : "+ sub.mkString(",")
val ins_out = "Insertions : "+ ins.mkString(",")
val del_out = "Deletions : "+ del.mkString(",")

val S_matrix = "S = ["+ sub.mkString(" ") + "]"
val I_matrix = "I = ["+ ins.mkString(" ") + "]"
val D_matrix = "D = ["+ del.mkString(" ") + "]"

//Currently we output the total number of different types of mutations.
val results = "# of insertions: " + insertions.count() + "\n# of deletions: " + deletions.count() + "\n# of substitutions: " + substitutions.count() +"\n"+ sub_out+"\n"+ ins_out+"\n"+ del_out
val mat_results = "X = ["+insertions.count()+" "+deletions.count()+" "+substitutions.count()+"];\n"+S_matrix+"\n"+I_matrix+"\n"+D_matrix+"\n"+"labels = {'Insertions','Deletions','Substitutions'};\npie(X,labels)"

//write results to a file, in this directory
File("task3_output").writeAll(results)
File("task3_chart.m").writeAll(mat_results)


