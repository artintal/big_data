def format_set(bins):
    s = '{'
    for i, b in enumerate(bins):
        s += "'"+str(b)+"'"
        if i+1 < len(bins):
            s += ','
    return s+'}'

def to_matlab(filename, output_file=None):
    import re
    line_pattern = re.compile(r'[\d|\[\d+,\d+\)]+: \d+')
    csv_pattern = re.compile(r'\[\d+,\d+\)')
    f = open(filename, 'r')
    lines = f.readlines()
    f.close()
    output_file = output_file or filename[:filename.rfind('.')]+'.m'
    f = open(output_file, 'w')
    queued = []
    counter = 0
    for line in lines:
        if re.match(line_pattern, line):
            key, value = line.split(': ')
            if re.match(csv_pattern, key):
                queued.append((key[1:-1].split(','), int(value)))
            else:
                queued.append(([key], int(value)))
        else:
            if len(queued) > 0:
                m = max(v for _, v in queued)
                bins = []
                values = []
                normalized = []
                for bs, v in queued:
                    bins.extend(int(b) for b in bs)
                    values.append(v)
                    normalized.append(float(v)/m)
                bins = sorted(set(bins))
                f.write('bins{c} = {b};\nvalues{c} = {v};\nnormalized{c} = {n};\n'.format(
                    c=counter,
                    b=format_set(bins) if counter <= 1 else repr(bins),
                    v=repr(values),
                    n=repr(normalized)
                ))
                queued = []
                counter += 1
            f.write('% '+line) # MATLAB comment
    if len(queued) > 0:
        m = max(v for _, v in queued)
        bins = []
        values = []
        normalized = []
        for bs, v in queued:
            bins.extend(int(b) for b in bs)
            values.append(v)
            normalized.append(float(v)/m)
        bins = sorted(set(bins))
        f.write('bins{c} = {b};\nvalues{c} = {v};\nnormalized{c} = {n};\n'.format(c=counter, b=repr(bins), v=repr(values), n=repr(normalized)))
        queued = []
        counter += 1
    f.write("\n%% Plots\nfigure();\nhistogram('Categories',bins0,'BinCounts',values0);\ntitle('Raw Counts');\naxis([0.5,23.5,0,max(values0)]);\nset(gca, 'FontSize', 36);\n")
    f.write("figure();\nhistogram('Categories',bins1,'BinCounts',values1);\ntitle('Loss of Function');\naxis([0.5,23.5,0,max(values1)]);\nset(gca, 'FontSize', 36);\n")
    f.write("figure();\n")
    for i in range(3, counter, 2):
        f.write("subplot(5, 5, {ch});\nhistogram('BinEdges',bins{c},'BinCounts',values{c});\naxis([min(bins{c}),max(bins{c}),0,max(values{c})]);\ntitle('Chromosome {ch}');\nset(gca, 'FontSize', 16);\n".format(c=i, ch=i//2))
    f.write("figure();\n")
    for i in range(2, counter, 2):
        f.write("subplot(5, 5, {ch});\nhistogram('BinEdges',bins{c},'BinCounts',values{c});\naxis([min(bins{c}),max(bins{c}),0,max(values{c})]);\ntitle('Chromosome {ch}');\nset(gca, 'FontSize', 16);\n".format(c=i, ch=i//2))
    # for i in range(3, counter, 2):
        # f.write("figure();\ntitle('Loss of Function Counts for Chromosome {c}');\n".format(c=i//2+1))
        # f.write("histogram('BinEdges',bins{c},'BinCounts',values{c});\n".format(c=i))
    # for i in range(2, counter, 2):
        # f.write("figure();\ntitle('Raw Counts for Chromosome {c}');\n".format(c=i//2+1))
        # f.write("histogram('BinEdges',bins{c},'BinCounts',values{c});\n".format(c=i))
    f.close()